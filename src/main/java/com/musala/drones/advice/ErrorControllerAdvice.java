package com.musala.drones.advice;

import com.musala.drones.enums.ResponseCode;
import com.musala.drones.exceptions.BadRequestException;
import com.musala.drones.exceptions.DroneOverWeightException;
import com.musala.drones.exceptions.InvalidDroneStateException;
import com.musala.drones.exceptions.LowDroneBatteryLevelException;
import com.musala.drones.models.APIResponse;
import com.musala.drones.util.BaseErrorResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ErrorControllerAdvice {

    private final BaseErrorResponse baseErrorResponse;

    private static List<String> getValidationMessage(BindingResult bindingResult) {
        return bindingResult.getAllErrors()
                .stream()
                .map(ErrorControllerAdvice::getValidationMessage)
                .collect(Collectors.toList());
    }

    private static String getValidationMessage(ObjectError error) {
        if (error instanceof FieldError fieldError) {
            String className = fieldError.getObjectName();
            String property = fieldError.getField();
            Object invalidValue = fieldError.getRejectedValue();
            String message = fieldError.getDefaultMessage();
            return String.format("%s.%s %s, but it was %s", className, property, message, invalidValue);
        }

        return String.format("%s: %s", error.getObjectName(), error.getDefaultMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public APIResponse handleJavaxValidationException(MethodArgumentNotValidException e) {
        String errorMessage = getValidationMessage(e.getBindingResult()).toString();
        return baseErrorResponse.buildErrorResponse(ResponseCode.BAD_REQUEST.getCode(), errorMessage, e);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public APIResponse handleBadRequestApiException(BadRequestException e) {
        return baseErrorResponse.buildErrorResponse(e.getCode(), e.getMessage(), e);
    }

    @ExceptionHandler(DroneOverWeightException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public APIResponse handleDroneLoadingException(DroneOverWeightException e) {
        return baseErrorResponse.buildErrorResponse(e.getCode(), e.getMessage(), e);
    }

    @ExceptionHandler(InvalidDroneStateException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public APIResponse handleDroneLoadingException(InvalidDroneStateException e) {
        return baseErrorResponse.buildErrorResponse(e.getCode(), e.getMessage(), e);
    }


    @ExceptionHandler(LowDroneBatteryLevelException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public APIResponse handleDroneLoadingException(LowDroneBatteryLevelException e) {
        return baseErrorResponse.buildErrorResponse(e.getCode(), e.getMessage(), e);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public APIResponse handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        return baseErrorResponse.buildErrorResponse(ResponseCode.BAD_REQUEST.getCode(), e.getMessage(), e);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public APIResponse handleGeneralException(Exception e) {
        log.error("Server error", e);
        return baseErrorResponse.buildErrorResponse("Oops, It's not you. Please try again later");
    }
}
