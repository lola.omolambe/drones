package com.musala.drones.enums;

public enum ResponseCode {

    // 400 Series
    BAD_REQUEST("4000", "The request could not be completed due to malformed syntax. Kindly crosscheck and try again."),
    DUPLICATE_SERIAL_NUMBER("4001", "A drone with this serial number already exists."),
    INVALID_SERIAL_NUMBER("4002", "A drone with this serial number does not exist."),
    INVALID_LOADING_STATE("4003", "Drone is not in idle state."),
    LOW_DRONE_BATTERY_LEVEL("4004", "Drone Battery level is currently lower than minimum allowed level"),
    DRONE_WEIGHT_LIMIT_EXCEEDED("4005", "Drone Weight Limit exceeded"),

    // 500 Series
    INTERNAL_SERVER_ERROR("5000", "An unexpected error occurred while processing your request. Please try again later.");

    private String code;

    private String description;

    ResponseCode() {
    }

    ResponseCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
