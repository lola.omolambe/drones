package com.musala.drones.modules.medication.repository;

import com.musala.drones.modules.medication.entity.Medication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    Page<Medication> findAllByDrone_SerialNumber(String serialNumber, Pageable pageable);
}