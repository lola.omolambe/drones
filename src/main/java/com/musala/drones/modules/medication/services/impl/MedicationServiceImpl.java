package com.musala.drones.modules.medication.services.impl;

import com.musala.drones.modules.drone.apimodels.MedicationRequest;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.medication.apimodels.MedicationResponse;
import com.musala.drones.modules.medication.entity.Medication;
import com.musala.drones.modules.medication.repository.MedicationRepository;
import com.musala.drones.modules.medication.services.MedicationService;
import com.musala.drones.util.MedicationConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {
    private final MedicationRepository medicationRepository;

    @Override
    public List<Medication> saveMedications(Drone drone, List<MedicationRequest> medicationRequests) {

        List<Medication> medicationList = medicationRequests.stream()
                .map(medicationRequest -> MedicationConverter.convertToMedicationModel(medicationRequest, drone))
                .toList();

        return medicationRepository.saveAll(medicationList);
    }

    @Override
    public Page<MedicationResponse> getMedicationItems(String droneSerialNumber, Pageable pageable) {
        return medicationRepository.findAllByDrone_SerialNumber(droneSerialNumber, pageable)
                .map(MedicationConverter::buildMedicationResponse);
    }
}
