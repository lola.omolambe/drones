package com.musala.drones.modules.medication.controller;

import com.musala.drones.modules.medication.apimodels.MedicationResponse;
import com.musala.drones.modules.medication.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/medications")
@RequiredArgsConstructor
public class MedicationController {
    private final MedicationService medicationService;

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<MedicationResponse> getAllMedicationItems(@RequestParam String serialNumber,
                                                          @PageableDefault(size = 20) Pageable pageable){
        return medicationService.getMedicationItems(serialNumber, pageable);
    }
}
