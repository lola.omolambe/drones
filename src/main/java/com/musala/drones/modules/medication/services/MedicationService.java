package com.musala.drones.modules.medication.services;

import com.musala.drones.modules.drone.apimodels.MedicationRequest;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.medication.apimodels.MedicationResponse;
import com.musala.drones.modules.medication.entity.Medication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MedicationService {
    List<Medication> saveMedications(Drone drone, List<MedicationRequest> medicationRequests);

    Page<MedicationResponse> getMedicationItems(String droneSerialNumber, Pageable pageable);
}
