package com.musala.drones.modules.medication.entity;


import com.musala.drones.entities.BaseEntity;
import com.musala.drones.modules.drone.entity.Drone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "medications")
public class Medication extends BaseEntity {
    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "drone_id", nullable = false)
    private Drone drone;

    private String name;

    private double weight;

    private String code;

    private String image;
}
