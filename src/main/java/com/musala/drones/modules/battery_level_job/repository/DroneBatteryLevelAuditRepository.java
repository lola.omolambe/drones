package com.musala.drones.modules.battery_level_job.repository;

import com.musala.drones.modules.battery_level_job.entity.DroneBatteryLevelAudit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneBatteryLevelAuditRepository extends JpaRepository<DroneBatteryLevelAudit, Long> {
}
