package com.musala.drones.modules.battery_level_job.services.impl;

import com.musala.drones.config.DroneConfigProperties;
import com.musala.drones.modules.battery_level_job.services.DroneBatteryLevelAuditService;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.drone.services.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class BatteryLevelLogJob {

    private final DroneService droneService;
    private final DroneBatteryLevelAuditService droneBatteryLevelAuditService;
    private final DroneConfigProperties droneConfigProperties;

    @Scheduled(cron = "#{@droneConfigProperties.batteryLevelAuditCronExpression}")
    public void runBatteryLevelCheck() {

        if(!droneConfigProperties.isRunBatteryLevelAuditJob()){
            log.debug("Battery level audit job disabled.");
            return;
        }

        log.debug("Running battery level audit job");

        Pageable pageRequest = PageRequest.of(0, droneConfigProperties.getJobRecordPageSize());
        boolean hasNextPage;

        do {

            Page<Drone> drones = droneService.getDrones(pageRequest);

            drones.stream().forEach(droneBatteryLevelAuditService::logAudit);

            hasNextPage = drones.hasNext();
            pageRequest = pageRequest.next();
        } while (hasNextPage);

        log.debug("Successfully ran battery level audit job");
    }

}
