package com.musala.drones.modules.battery_level_job.entity;

import com.musala.drones.entities.BaseEntity;
import com.musala.drones.modules.drone.entity.Drone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "drone_battery_level_audit_logs")
public class DroneBatteryLevelAudit extends BaseEntity {

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "drone_id", nullable = false)
    private Drone drone;

    @Column(nullable = false, precision = 5, scale = 2)
    private BigDecimal batteryCapacity;
}
