package com.musala.drones.modules.battery_level_job.services;

import com.musala.drones.modules.drone.entity.Drone;

public interface DroneBatteryLevelAuditService {
    void logAudit(Drone drone);
}
