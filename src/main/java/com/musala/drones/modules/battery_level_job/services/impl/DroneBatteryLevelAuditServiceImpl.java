package com.musala.drones.modules.battery_level_job.services.impl;

import com.musala.drones.modules.battery_level_job.entity.DroneBatteryLevelAudit;
import com.musala.drones.modules.battery_level_job.repository.DroneBatteryLevelAuditRepository;
import com.musala.drones.modules.battery_level_job.services.DroneBatteryLevelAuditService;
import com.musala.drones.modules.drone.entity.Drone;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DroneBatteryLevelAuditServiceImpl implements DroneBatteryLevelAuditService {
    private final DroneBatteryLevelAuditRepository droneBatteryLevelAuditRepository;

    @Override
    public void logAudit(Drone drone) {
        DroneBatteryLevelAudit batteryLevelAudit = DroneBatteryLevelAudit.builder()
                .batteryCapacity(drone.getBatteryCapacity())
                .drone(drone).build();

        droneBatteryLevelAuditRepository.save(batteryLevelAudit);
    }
}
