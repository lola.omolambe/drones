package com.musala.drones.modules.drone.controller;

import com.musala.drones.modules.drone.apimodels.DroneResponse;
import com.musala.drones.modules.drone.apimodels.LoadDroneRequest;
import com.musala.drones.modules.drone.apimodels.RegisterDroneRequest;
import com.musala.drones.modules.drone.services.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/drones")
@RequiredArgsConstructor
public class DroneController {
    private final DroneService droneService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DroneResponse registerDrone(@RequestBody @Validated RegisterDroneRequest registerDroneRequest) {
        return droneService.registerDrone(registerDroneRequest);
    }

    @PostMapping(path = "/load", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void loadDrone(@RequestBody @Validated LoadDroneRequest loadDroneRequest) {
        droneService.loadDrone(loadDroneRequest);
    }

    @GetMapping(path = "/available", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<DroneResponse> getAvailableDronesForLoading(@PageableDefault(size = 20) Pageable pageable) {
        return droneService.getAvailableDronesForLoading(pageable);
    }

    @GetMapping(path = "/battery-level", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DroneResponse getDroneBatteryLevel(@RequestParam String serialNumber){
        return droneService.getDroneBatteryLevel(serialNumber);
    }
}
