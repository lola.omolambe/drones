package com.musala.drones.modules.drone.services;

import com.musala.drones.modules.drone.apimodels.DroneResponse;
import com.musala.drones.modules.drone.apimodels.LoadDroneRequest;
import com.musala.drones.modules.drone.apimodels.RegisterDroneRequest;
import com.musala.drones.modules.drone.entity.Drone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DroneService {
    DroneResponse registerDrone(RegisterDroneRequest registerDroneRequest);

    void loadDrone(LoadDroneRequest loadDroneRequest);

    Page<DroneResponse> getAvailableDronesForLoading(Pageable pageable);

    DroneResponse getDroneBatteryLevel(String serialNumber);

    Page<Drone> getDrones(Pageable pageable);
}
