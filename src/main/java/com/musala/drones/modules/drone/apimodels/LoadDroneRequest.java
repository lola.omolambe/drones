package com.musala.drones.modules.drone.apimodels;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoadDroneRequest {
    @NotBlank(message = "Drone serial number is required.")
    private String droneSerialNumber;

    @Valid
    private List<MedicationRequest> medicationRequests;
}