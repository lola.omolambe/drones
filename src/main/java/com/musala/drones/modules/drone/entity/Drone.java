package com.musala.drones.modules.drone.entity;

import com.musala.drones.entities.BaseEntity;
import com.musala.drones.modules.drone.enums.DroneModel;
import com.musala.drones.modules.drone.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "drones")
public class Drone extends BaseEntity {
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private DroneModel model;

    private double weightLimit;

    private BigDecimal batteryCapacity;

    @Enumerated(EnumType.STRING)
    private DroneState state;

    private double currentWeight;
}

