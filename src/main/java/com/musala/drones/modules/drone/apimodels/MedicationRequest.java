package com.musala.drones.modules.drone.apimodels;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MedicationRequest {
    @NotBlank(message = "Medication name is required.")
    @Pattern(regexp = "^[A-Za-z0-9_-]*$", message = "Medication name can only contain letters, numbers, ‘-‘, ‘_’")
    private String name;

    @NotNull(message = "Medication weight is required.")
    private Double weight;

    @Pattern(regexp = "^[A-Z0-9_]*$", message = "Medication code can contain only upper case letters, underscore and numbers.")
    private String code;

    @NotBlank(message = "Medication image is required.")
    private String image;
}
