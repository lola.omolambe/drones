package com.musala.drones.modules.drone.apimodels;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.musala.drones.modules.drone.enums.DroneModel;
import com.musala.drones.modules.drone.enums.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DroneResponse {
    private String serialNumber;

    private DroneModel model;

    private Double weightLimit;

    private BigDecimal batteryCapacity;

    private DroneState state;

    private Double currentWeight;
}