package com.musala.drones.modules.drone.enums;

public enum DroneModel {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}