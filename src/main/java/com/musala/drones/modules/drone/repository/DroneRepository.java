package com.musala.drones.modules.drone.repository;

import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.drone.enums.DroneState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DroneRepository extends JpaRepository<Drone, Long> {
    Optional<Drone> findBySerialNumber(String serialNumber);

    Page<Drone> findAllByState(DroneState state, Pageable pageable);
}
