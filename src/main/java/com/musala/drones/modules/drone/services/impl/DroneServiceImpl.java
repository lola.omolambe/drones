package com.musala.drones.modules.drone.services.impl;

import com.musala.drones.enums.ResponseCode;
import com.musala.drones.exceptions.BadRequestException;
import com.musala.drones.exceptions.DroneOverWeightException;
import com.musala.drones.exceptions.InvalidDroneStateException;
import com.musala.drones.exceptions.LowDroneBatteryLevelException;
import com.musala.drones.modules.drone.apimodels.DroneResponse;
import com.musala.drones.modules.drone.apimodels.LoadDroneRequest;
import com.musala.drones.modules.drone.apimodels.MedicationRequest;
import com.musala.drones.modules.drone.apimodels.RegisterDroneRequest;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.drone.enums.DroneState;
import com.musala.drones.modules.drone.repository.DroneRepository;
import com.musala.drones.modules.drone.services.DroneService;
import com.musala.drones.modules.medication.services.MedicationService;
import com.musala.drones.util.DroneConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static com.musala.drones.modules.drone.enums.DroneState.IDLE;
import static com.musala.drones.modules.drone.enums.DroneState.LOADED;

@Service
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {
    private static final BigDecimal BATTERY_LEVEL = new BigDecimal(25);
    private final DroneRepository droneRepository;
    private final MedicationService medicationService;

    @Override
    public DroneResponse registerDrone(RegisterDroneRequest registerDroneRequest) {
        validateDroneSerialNumberIsUnique(registerDroneRequest);

        Drone drone = DroneConverter.convertToDroneModel(registerDroneRequest);

        Drone savedDrone = droneRepository.save(drone);

        return DroneConverter.convertToDroneResponse(savedDrone);
    }

    @Transactional
    @Override
    public void loadDrone(LoadDroneRequest loadDroneRequest) {
        Drone drone = getDrone(loadDroneRequest.getDroneSerialNumber());

        double totalMedicationWeight = getTotalMedicationWeight(loadDroneRequest.getMedicationRequests());

        validateDroneIsInIdleState(drone.getState());
        validateDroneBatteryLevelIsGreaterThanAllowedLevel(drone.getBatteryCapacity());
        validateDroneCanTakeMedicationWeight(drone.getWeightLimit(), drone.getCurrentWeight(), totalMedicationWeight);

        medicationService.saveMedications(drone, loadDroneRequest.getMedicationRequests());

        updateDroneDetails(drone, totalMedicationWeight);
    }

    @Override
    public Page<DroneResponse> getAvailableDronesForLoading(Pageable pageable) {
        return droneRepository.findAllByState(IDLE, pageable)
                .map(DroneConverter::convertToDroneResponse);
    }

    @Override
    public DroneResponse getDroneBatteryLevel(String serialNumber) {
        Drone drone = getDrone(serialNumber);

        return DroneResponse.builder().batteryCapacity(drone.getBatteryCapacity()).build();
    }

    @Override
    public Page<Drone> getDrones(Pageable pageable) {
        return droneRepository.findAll(pageable);
    }

    private Drone getDrone(String serialNumber) {
        return droneRepository.findBySerialNumber(serialNumber)
                .orElseThrow(() -> new BadRequestException(
                        String.format("%s :%s", ResponseCode.INVALID_SERIAL_NUMBER.getDescription(), serialNumber)));
    }

    private double getTotalMedicationWeight(List<MedicationRequest> medicationRequests) {
        return medicationRequests.stream().map(MedicationRequest::getWeight)
                .reduce(0.0, Double::sum);
    }

    private void updateDroneDetails(Drone drone, double totalMedicationWeight) {
        drone.setCurrentWeight(drone.getCurrentWeight() + totalMedicationWeight);
        drone.setState(LOADED);
        droneRepository.save(drone);
    }

    private void validateDroneCanTakeMedicationWeight(double weightLimit, double currentWeight, double medicationsWeight) {
        if (!(weightLimit >= (currentWeight + medicationsWeight))) {
            throw new DroneOverWeightException();
        }
    }

    private void validateDroneBatteryLevelIsGreaterThanAllowedLevel(BigDecimal batteryLevel) {
        if (BATTERY_LEVEL.compareTo(batteryLevel) > 0) {
            throw new LowDroneBatteryLevelException();
        }
    }

    private void validateDroneIsInIdleState(DroneState state) {
        if (!(IDLE.equals(state))) {
            throw new InvalidDroneStateException();
        }
    }

    private void validateDroneSerialNumberIsUnique(RegisterDroneRequest registerDroneRequest) {
        droneRepository.findBySerialNumber(registerDroneRequest.getSerialNumber())
                .ifPresent(drone -> {
                    throw new BadRequestException(ResponseCode.DUPLICATE_SERIAL_NUMBER.getDescription());
                });
    }
}
