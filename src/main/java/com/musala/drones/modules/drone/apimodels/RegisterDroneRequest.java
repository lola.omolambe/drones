package com.musala.drones.modules.drone.apimodels;

import com.musala.drones.modules.drone.enums.DroneModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDroneRequest {
    @NotBlank(message = "Serial number is required.")
    @Size(max = 100, message = "Serial number max character size is 100.")
    private String serialNumber;

    @NotNull(message = "Drone model is required.")
    private DroneModel model;

    @NotNull(message = "Weight Limit is required.")
    @Min(value = 0, message = "Weight limit must be greater than 0gr.")
    @Max(value = 500, message = "Weight limit must not be greater than 500gr.")
    private Double weightLimit;
}
