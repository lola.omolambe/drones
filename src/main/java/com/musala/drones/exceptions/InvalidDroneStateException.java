package com.musala.drones.exceptions;


import com.musala.drones.enums.ResponseCode;

public class InvalidDroneStateException extends RuntimeException {
    private final String code;

    public InvalidDroneStateException() {
        super(ResponseCode.INVALID_LOADING_STATE.getDescription());
        this.code = ResponseCode.INVALID_LOADING_STATE.getCode();
    }

    public InvalidDroneStateException(ResponseCode responseCode) {
        super(responseCode.getDescription());
        this.code = responseCode.getCode();
    }

    public InvalidDroneStateException(String message) {
        super(message);
        this.code = ResponseCode.INVALID_LOADING_STATE.getCode();
    }

    public InvalidDroneStateException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
