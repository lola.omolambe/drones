package com.musala.drones.exceptions;


import com.musala.drones.enums.ResponseCode;

public class LowDroneBatteryLevelException extends RuntimeException {
    private final String code;

    public LowDroneBatteryLevelException() {
        super(ResponseCode.LOW_DRONE_BATTERY_LEVEL.getDescription());
        this.code = ResponseCode.LOW_DRONE_BATTERY_LEVEL.getCode();
    }

    public LowDroneBatteryLevelException(ResponseCode responseCode) {
        super(responseCode.getDescription());
        this.code = responseCode.getCode();
    }

    public LowDroneBatteryLevelException(String message) {
        super(message);
        this.code = ResponseCode.LOW_DRONE_BATTERY_LEVEL.getCode();
    }

    public LowDroneBatteryLevelException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
