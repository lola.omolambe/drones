package com.musala.drones.exceptions;


import com.musala.drones.enums.ResponseCode;

public class DroneOverWeightException extends RuntimeException {
    private final String code;

    public DroneOverWeightException() {
        super(ResponseCode.DRONE_WEIGHT_LIMIT_EXCEEDED.getDescription());
        this.code = ResponseCode.DRONE_WEIGHT_LIMIT_EXCEEDED.getCode();
    }

    public DroneOverWeightException(ResponseCode responseCode) {
        super(responseCode.getDescription());
        this.code = responseCode.getCode();
    }

    public DroneOverWeightException(String message) {
        super(message);
        this.code = ResponseCode.DRONE_WEIGHT_LIMIT_EXCEEDED.getCode();
    }

    public DroneOverWeightException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
