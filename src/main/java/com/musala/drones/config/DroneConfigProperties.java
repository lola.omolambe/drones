package com.musala.drones.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "drone")
public class DroneConfigProperties {
    private int jobRecordPageSize = 200;

    private boolean runBatteryLevelAuditJob = true;

    private String batteryLevelAuditCronExpression = "0 * * * * ?";
}
