package com.musala.drones.util;

import com.musala.drones.modules.drone.apimodels.MedicationRequest;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.medication.apimodels.MedicationResponse;
import com.musala.drones.modules.medication.entity.Medication;

public class MedicationConverter {

    public static Medication convertToMedicationModel(MedicationRequest medicationRequest, Drone drone) {
        return Medication.builder()
                .name(medicationRequest.getName())
                .code(medicationRequest.getCode())
                .weight(medicationRequest.getWeight())
                .image(medicationRequest.getImage())
                .drone(drone)
                .build();
    }

    public static MedicationResponse buildMedicationResponse(Medication medication) {
        return MedicationResponse.builder()
                .code(medication.getCode())
                .name(medication.getName())
                .image(medication.getImage())
                .weight(medication.getWeight())
                .build();
    }
}
