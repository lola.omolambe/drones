package com.musala.drones.util;

import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.drone.enums.DroneState;
import com.musala.drones.modules.drone.apimodels.DroneResponse;
import com.musala.drones.modules.drone.apimodels.RegisterDroneRequest;

import java.math.BigDecimal;

public class DroneConverter {
    private static final int DEFAULT_WEIGHT = 0;
    private static final BigDecimal DEFAULT_BATTERY_CAPACITY = new BigDecimal(100);

    public static Drone convertToDroneModel(RegisterDroneRequest registerDroneRequest) {
        return Drone.builder()
                .serialNumber(registerDroneRequest.getSerialNumber())
                .model(registerDroneRequest.getModel())
                .weightLimit(registerDroneRequest.getWeightLimit())
                .currentWeight(DEFAULT_WEIGHT)
                .state(DroneState.IDLE)
                .batteryCapacity(DEFAULT_BATTERY_CAPACITY)
                .build();
    }

    public static DroneResponse convertToDroneResponse(Drone drone) {
        return DroneResponse.builder()
                .serialNumber(drone.getSerialNumber())
                .model(drone.getModel())
                .weightLimit(drone.getWeightLimit())
                .currentWeight(drone.getCurrentWeight())
                .batteryCapacity(drone.getBatteryCapacity())
                .state(drone.getState())
                .build();
    }
}
