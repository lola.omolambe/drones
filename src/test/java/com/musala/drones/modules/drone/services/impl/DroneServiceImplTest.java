package com.musala.drones.modules.drone.services.impl;

import com.musala.drones.TestUtil;
import com.musala.drones.exceptions.BadRequestException;
import com.musala.drones.exceptions.DroneOverWeightException;
import com.musala.drones.exceptions.InvalidDroneStateException;
import com.musala.drones.exceptions.LowDroneBatteryLevelException;
import com.musala.drones.modules.drone.apimodels.DroneResponse;
import com.musala.drones.modules.drone.apimodels.LoadDroneRequest;
import com.musala.drones.modules.drone.apimodels.RegisterDroneRequest;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.drone.enums.DroneModel;
import com.musala.drones.modules.drone.enums.DroneState;
import com.musala.drones.modules.drone.repository.DroneRepository;
import com.musala.drones.modules.medication.entity.Medication;
import com.musala.drones.modules.medication.services.MedicationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.musala.drones.TestUtil.buildDrone;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DroneServiceImplTest {

    @Mock
    private DroneRepository droneRepository;

    @Mock
    private MedicationService medicationService;

    @InjectMocks
    private DroneServiceImpl droneService;

    private RegisterDroneRequest registerDroneRequest;

    private Drone drone;

    private LoadDroneRequest loadDroneRequest;

    @BeforeEach
    void setUp() {
        registerDroneRequest = TestUtil.buildRegisterDroneRequest();
        drone = buildDrone(registerDroneRequest);
        loadDroneRequest = TestUtil.buildLoadDroneRequest();
    }

    @Test
    void registerDroneFails_whenSerialNumberAlreadyExists() {
        when(droneRepository.findBySerialNumber(any()))
                .thenReturn(Optional.of(drone));

        assertThatThrownBy(() -> droneService.registerDrone(registerDroneRequest))
                .isInstanceOf(BadRequestException.class)
                .hasMessage("A drone with this serial number already exists.");
    }

    @Test
    void registerDroneSuccessfully() {
        when(droneRepository.findBySerialNumber(any()))
                .thenReturn(Optional.empty());
        when(droneRepository.save(any()))
                .thenReturn(drone);

        DroneResponse droneResponse = droneService.registerDrone(registerDroneRequest);

        assertEquals(registerDroneRequest.getModel(), droneResponse.getModel());
        assertEquals(registerDroneRequest.getWeightLimit(), droneResponse.getWeightLimit());
        assertEquals(new BigDecimal(100), droneResponse.getBatteryCapacity());
        assertEquals(DroneState.IDLE, droneResponse.getState());
        assertEquals(registerDroneRequest.getSerialNumber(), droneResponse.getSerialNumber());
    }

    @Test
    void loadDroneFails_whenDroneSerialNumberDoesNotExist() {
        when(droneRepository.findBySerialNumber(any()))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> droneService.loadDrone(loadDroneRequest))
                .isInstanceOf(BadRequestException.class)
                .hasMessage("A drone with this serial number does not exist. :3456664");
    }

    @Test
    void loadDroneFails_whenDroneIsNotInIdleState() {
        drone.setState(DroneState.LOADED);

        when(droneRepository.findBySerialNumber(any()))
                .thenReturn(Optional.of(drone));

        assertThatThrownBy(() -> droneService.loadDrone(loadDroneRequest))
                .isInstanceOf(InvalidDroneStateException.class)
                .hasMessage("Drone is not in idle state.");
    }

    @Test
    void loadDroneFails_whenDroneBatteryLevelIsLow() {
        drone.setBatteryCapacity(BigDecimal.valueOf(24));

        when(droneRepository.findBySerialNumber(any()))
                .thenReturn(Optional.of(drone));

        assertThatThrownBy(() -> droneService.loadDrone(loadDroneRequest))
                .isInstanceOf(LowDroneBatteryLevelException.class)
                .hasMessage("Drone Battery level is currently lower than minimum allowed level");
    }

    @Test
    void loadDroneFails_whenDroneWeightIsExceeded() {
        drone.setWeightLimit(20.0);

        when(droneRepository.findBySerialNumber(any()))
                .thenReturn(Optional.of(drone));

        assertThatThrownBy(() -> droneService.loadDrone(loadDroneRequest))
                .isInstanceOf(DroneOverWeightException.class)
                .hasMessage("Drone Weight Limit exceeded");
    }

    @Test
    void loadDroneSuccessfully() {
        Medication medication = TestUtil.buildMedication();
        List<Medication> medications = Collections.singletonList(medication);

        when(droneRepository.findBySerialNumber(any()))
                .thenReturn(Optional.of(drone));
        when(medicationService.saveMedications(any(), any()))
                .thenReturn(medications);

        droneService.loadDrone(loadDroneRequest);

        verify(droneRepository).save(any());
    }


    @Test
    void getAvailableDronesForLoading() {
        List<Drone> drones = Collections.singletonList(drone);
        PageImpl<Drone> dronePage = new PageImpl<>(drones);

        when(droneRepository.findAllByState(any(), any()))
                .thenReturn(dronePage);

        Page<DroneResponse> droneResponses = droneService.getAvailableDronesForLoading(PageRequest.of(0, 20));

        assertEquals(DroneModel.Middleweight, droneResponses.getContent().get(0).getModel());
        assertEquals(400.0, droneResponses.getContent().get(0).getWeightLimit());
        assertEquals(new BigDecimal(100), droneResponses.getContent().get(0).getBatteryCapacity());
        assertEquals(DroneState.IDLE, droneResponses.getContent().get(0).getState());
        assertEquals("3456664", droneResponses.getContent().get(0).getSerialNumber());
    }

    @Test
    void getDroneBatteryLevelFails_whenSerialNumberDoesNotExist() {
        when(droneRepository.findBySerialNumber("123456"))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> droneService.getDroneBatteryLevel("123456"))
                .isInstanceOf(BadRequestException.class)
                .hasMessage("A drone with this serial number does not exist. :123456");
    }

    @Test
    void getDroneBatteryLevelSuccessfully() {
        when(droneRepository.findBySerialNumber("123456"))
                .thenReturn(Optional.of(drone));

        DroneResponse droneResponse = droneService.getDroneBatteryLevel("123456");

        assertNull(droneResponse.getModel());
        assertNull(droneResponse.getWeightLimit());
        assertNull(droneResponse.getState());
        assertNull(droneResponse.getSerialNumber());

        assertEquals(new BigDecimal(100), droneResponse.getBatteryCapacity());
    }

}