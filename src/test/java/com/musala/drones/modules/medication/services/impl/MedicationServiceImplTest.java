package com.musala.drones.modules.medication.services.impl;

import com.musala.drones.TestUtil;
import com.musala.drones.modules.drone.apimodels.MedicationRequest;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.medication.apimodels.MedicationResponse;
import com.musala.drones.modules.medication.entity.Medication;
import com.musala.drones.modules.medication.repository.MedicationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicationServiceImplTest {

    @Mock
    private MedicationRepository medicationRepository;

    @InjectMocks
    private MedicationServiceImpl medicationService;

    private Drone drone;

    @BeforeEach
    void setUp() {
        drone = TestUtil.buildDrone();
    }

    @Test
    void saveMedicationsSuccessfully() {
        List<MedicationRequest> medicationRequests = Collections.singletonList(TestUtil.buildMedicationRequest());

        Medication medication = TestUtil.buildMedication();
        List<Medication> medicationList = Collections.singletonList(medication);

        when(medicationRepository.saveAll(any()))
                .thenReturn(medicationList);

        List<Medication> medications = medicationService.saveMedications(drone, medicationRequests);

        assertEquals(1, medications.size());
        assertEquals("Medication", medications.get(0).getName());
        assertEquals("CODE", medications.get(0).getCode());
        assertEquals(30.0, medications.get(0).getWeight());
    }

    @Test
    void getMedicationItems() {
        Medication medication = TestUtil.buildMedication();
        PageImpl<Medication> medicationPage = new PageImpl<>(Collections.singletonList(medication));

        when(medicationRepository.findAllByDrone_SerialNumber(any(), any()))
                .thenReturn(medicationPage);

        Page<MedicationResponse> medicationResponses = medicationService.getMedicationItems("12345", PageRequest.of(0, 5));

        assertEquals(1, medicationResponses.getTotalElements());
        assertEquals("Medication", medicationResponses.getContent().get(0).getName());
        assertEquals("CODE", medicationResponses.getContent().get(0).getCode());
        assertEquals(30.0, medicationResponses.getContent().get(0).getWeight());
    }


}