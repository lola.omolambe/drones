package com.musala.drones;

import com.musala.drones.modules.drone.apimodels.LoadDroneRequest;
import com.musala.drones.modules.drone.apimodels.MedicationRequest;
import com.musala.drones.modules.drone.apimodels.RegisterDroneRequest;
import com.musala.drones.modules.drone.entity.Drone;
import com.musala.drones.modules.drone.enums.DroneModel;
import com.musala.drones.modules.drone.enums.DroneState;
import com.musala.drones.modules.medication.entity.Medication;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

public class TestUtil {
    public static RegisterDroneRequest buildRegisterDroneRequest() {
        return RegisterDroneRequest.builder().model(DroneModel.Middleweight).serialNumber("3456664").weightLimit(400.0).build();
    }

    public static Drone buildDrone(RegisterDroneRequest registerDroneRequest) {
        return Drone.builder()
                .serialNumber(registerDroneRequest.getSerialNumber())
                .model(registerDroneRequest.getModel())
                .weightLimit(registerDroneRequest.getWeightLimit())
                .currentWeight(20.0)
                .state(DroneState.IDLE)
                .batteryCapacity(new BigDecimal(100))
                .build();
    }

    public static Drone buildDrone() {
        return Drone.builder()
                .serialNumber("3456664")
                .model(DroneModel.Middleweight)
                .weightLimit(300)
                .currentWeight(20.0)
                .state(DroneState.IDLE)
                .batteryCapacity(new BigDecimal(100))
                .build();
    }

    public static LoadDroneRequest buildLoadDroneRequest() {
        MedicationRequest medicationRequest = buildMedicationRequest();
        List<MedicationRequest> medicationRequests = Collections.singletonList(medicationRequest);
        return LoadDroneRequest.builder().droneSerialNumber("3456664").medicationRequests(medicationRequests).build();
    }

    public static MedicationRequest buildMedicationRequest() {
        return MedicationRequest.builder().name("Medication").code("CODE").weight(30.0).build();
    }

    public static Medication buildMedication() {
        return Medication.builder().name("Medication").code("CODE").weight(30.0).build();
    }
}
